if platform?("amazon")
  default["vpcnat"]["scripts"]["aws"] = "/usr/bin/aws"
else
  default["vpcnat"]["scripts"]["aws"] = "/usr/local/bin/aws"
end

# access to AWS (optional)
default["vpcnat"]["aws-key"] = ""
default["vpcnat"]["aws-secret-key"] = ""

# Private Network and Private Route Table
default["vpcnat"]["private-net"] = "10.0.1.0/24"
default["vpcnat"]["route-table-id"] = "rtb-c5ae2fa0"
