#
# Cookbook Name:: vpcnat
# Recipe:: configure-nat
#
# Copyright (c) 2015 The Authors, All Rights Reserved.

include_recipe 'awscli'

instance_region = VPCNAT.get_instance_region
instance_id = VPCNAT.get_instance_id
route_table = node["vpcnat"]["route-table-id"]

# ENV['AWS_ACCESS_KEY_ID'] = "#{node["vpcnat"]["aws-key"]}"
# ENV['AWS_SECRET_ACCESS_KEY'] = "#{node["vpcnat"]["aws-secret-key"]}"

execute 'disable Source/dest. check' do
  command <<-EOH
    aws ec2 \
      --region '#{instance_region}' \
      modify-instance-attribute \
        --instance-id '#{instance_id}' \
        --no-source-dest-check
  EOH
end

execute 'update route table' do
  command <<-EOH
    aws ec2 create-route \
    --region '#{instance_region}' \
    --route-table-id '#{route_table}' \
    --destination-cidr-block 0.0.0.0/0 \
    --instance-id '#{instance_id}' || \
    aws ec2 replace-route \
    --region '#{instance_region}' \
    --route-table-id '#{route_table}' \
    --destination-cidr-block 0.0.0.0/0 \
    --instance-id '#{instance_id}'
  EOH
end

