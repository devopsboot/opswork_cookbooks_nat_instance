vpcnat Cookbook
=============

This cookbook configures your AWS instances to enable NAT 


Requirements
------------

* Chef (11.10 or higher)
* Berkshelf
* jq - JSON processor


AWS Credentials
===============

In order to manage AWS components, authentication credentials need to
be available to the node. There are 2 way to handle this:

1. explicitly pass credentials parameter to the resource
1. or let the resource pick up credentials from the IAM role assigned to the instance


(see : http://docs.aws.amazon.com/cli/latest/userguide/cli-chap-getting-started.html#cli-installing-credentials)


Recipes
========

vpcnat::default
---------------

- execute all of recipes below.

Usage
-----

Just include `vpcnat` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[vpcnat]"
  ]
}
```